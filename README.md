## About

Provides a collection of Migrate plugins that are not part
of the Migrate core or Migrate Plus plugins.

### Process plugins

- country_code - Gets the ISO ISO 3166-1 alpha-2 country code from a country name, with optional locale configuration, requires commerceguys/addressing and commerceguys/intl, used by the Address module.
- format_phone (see #2889576) - Formats a phone number based on Google's libphonenumber.
- file_image_to_media: Generates a media entity from an image file and returns the media id.
- file_document_to_media: Generates a media entity from a document file and returns the media id.
- validate_email - Validates email address via the core email.validator service, with optional MX / A lookup.
- validate_link - Checks if a link does not return a 404 header.
- skip_on_user_duplicate_value -  Skips if the User entity value is not unique. Valid values: mail or name.
- skip_on_contains -  If the source evaluates to a contained string value, skip processing or whole row.
- skip_on_start_with - If the source evaluates to a starting string value, skip processing or whole row.
- integer_extract - Extracts the integer from a string (e.g. "1234 City")
- wrapper_extract - Extracts a value from a wrapper string defined via configuration: [ ], ( ), ...
- wrapper_remove - Removes a value from a wrapper, and the wrapper itself.

### Node alias to redirect

Source plugin that can be used to convert D7 Node aliases to redirects.

### Roadmap / WIP

- format_integer (see #2916471) - Formats an integer number by removing thousands separators, prefix and suffix.
- format_decimal (see #2916472) - Formats a decimal number by removing thousands separators, prefix and suffix.
- format_name (Formats a name string, like first and last name and provide option for nobiliary particle).
- sanitize_text (see #2893221) - Facade on various text sanitization methods, like Word special char treatment, XSS filtering, HTML tidy, ...
- paragraphs (see #2977080) - Imports a single or multiple collection of fields to Paragraphs.
- synonym (see #2889388) - Merges several values for entity reference.

Example use case for synonym: several values are describing the same reality, and we want to define them as the same taxonomy term, this can be also useful to cover typos.

### Credits

- File to Media process plugins are inspired from Think Tandem
[blog post](https://thinktandem.io/blog/2019/04/04/migrating-a-drupal-7-file-to-a-drupal-8-media-entity/)
- Node alias to redirect is inspired from @socketwench
[blog post](https://deninet.com/blog/2018/04/22/migrating-path-aliases-drupal-8-redirects-part-2).

Thank you for sharing!

## Documentation

### country_code

Example Migrate process plugin definition for the Address module,
from several source fields, with a country name field
defined in English that is converted in a country code.

```yaml
  field_address/country_code:
    plugin: country_code
    source: country_name_en
    locale: en
  field_address/locality: city
  field_address/postal_code: zip
  field_address/address_line1: address
```

### format_phone

[Documentation](https://github.com/giggsey/libphonenumber-for-php/blob/master/docs/PhoneNumberUtil.md])

```yaml
  field_telephone:
    plugin: format_phone
    source: head_office_phone
    region: EU
```

### file_image_to_media

Prerequisite: already migrated files.

```yaml
  field_media_reference_name:
    -
      plugin: sub_process
      source: field_name_from_d7_source
      process:
        target_id:
          -
            plugin: migration_lookup
            source: fid
            migration: upgrade_d7_file
          -
            plugin: file_image_to_media
            bundle: image
            field: field_media_file
            name: file/title
            delete_original: true
```

### file_document_to_media

Prerequisite: already migrated files.

```yaml
  field_media_reference_name:
    -
      plugin: sub_process
      source: field_name_from_d7_source
      process:
        target_id:
          -
            plugin: migration_lookup
            source: fid
            migration: upgrade_d7_file
          -
            plugin: file_document_to_media
            bundle: document
            field: field_media_file
            name: file/title
            delete_original: true
```

### validate_email

Example with an optional DNS validator.
It checks the MX record first with a fallback to the A (or AAAA)' record.
If one of the two is valid, the test passes.

```yaml
  field_email:
    plugin: validate_email
    source: email
    extra_validator: dns
```

### validate_link

```yaml
  field_website:
    plugin: validate_link
    source: website
```

### skip_on_user_duplicate_value

```yaml
  mail:
    plugin: skip_on_user_duplicate_value
    source: email
    method: row
    value: mail
```

Combined example: 
Skip on empty or duplicate, validate mail and set lowercase.

```yaml
mail:
    -
      plugin: skip_on_empty
      method: row
      source: email
    -
      plugin: skip_on_user_duplicate_value
      source: email
      method: row
      value: mail
    -
      plugin: validate_email
      source: email
    -
      plugin: callback
      callable: strtolower
```

### skip_on_contains

Minimal configuration

```yaml
  title:
    plugin: skip_on_contains
    source: title
    method: process
    value: 'spam'
```

Full configuration

```yaml
  title:
    plugin: skip_on_contains
    not_equals: true
    source: title
    method: row
    value:
      - 'to migrate'
      - 'to review'
```

### skip_on_start_with

Minimal configuration

```yaml
  title:
    plugin: skip_on_start_with
    source: title
    method: process
    value: 'Not to migrate -'
```

Full configuration

```yaml
  title:
    plugin: skip_on_start_with
    not_equals: true
    source: title
    method: row
    value:
      - 'To migrate -'
      - 'To review -'
```

### integer_extract

Can be used to extract e.g. zip codes.

Example transformation
* 1234 -> 1234
* "1234 City" -> 1234
* "1234.5 City" -> 12345

```yaml
  field_address/postal_code:
    plugin: integer_extract
    source: zip_and_city_source
```

### wrapper_extract

Example for a name that contains an acronym defined between parentheses:
e.g. _'European Union (EU)'_.
Define **open** as _'('_ and **close** as _')'_ to get the _'EU'_ string.
Also possible for other cases like _'&lt;tag&gt;'_ and _'&lt;'/tag&gt;'_
in some situations, when tags are enclosed in the string.

```yaml
  field_acronym:
    plugin: wrapper_extract
    source: name
    open: (
    close: )
```

### wrapper_remove

Example for a name that contains an acronym defined between parentheses:
e.g. _'European Union (EU)'_.
Define **open** as _'('_ and **close** as _')'_ to get the
_'European Union'_ string.

```yaml
  title:
    plugin: wrapper_remove
    source: name
    open: (
    close: )
```
