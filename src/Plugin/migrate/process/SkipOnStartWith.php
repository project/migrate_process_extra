<?php

namespace Drupal\migrate_process_extra\Plugin\migrate\process;

/**
 * If the source evaluates to a starting string value, skip processing or whole row.
 *
 * @MigrateProcessPlugin(
 *   id = "skip_on_start_with"
 * )
 *
 * Available configuration keys:
 * - value: An single value or array of values against which the source value
 *   should be compared.
 * - not_equals: (optional) If set, skipping occurs when values are not equal.
 * - method: What to do if the input value equals to value given in
 *   configuration key value. Possible values:
 *   - row: Skips the entire row.
 *   - process: Prevents further processing of the input property
 *
 * @codingStandardsIgnoreStart
 *
 * Examples:
 *
 * Example usage with minimal configuration:
 * @code
 *   title:
 *     plugin: skip_on_start_with
 *     source: title
 *     method: process
 *     value: 'Not to migrate -'
 * @endcode
 * The above example will skip further processing of the input property if
 * the 'title' source field starts with "Not to migrate -".
 *
 * Example usage with full configuration:
 * @code
 *   title:
 *     plugin: skip_on_start_with
 *     not_equals: true
 *     source: title
 *     method: row
 *     value:
 *       - 'To migrate -'
 *       - 'To review -'
 * @endcode
 * The above example will skip processing any row for which the source row's
 * 'title' source field is not "To migrate -" or "To review -".
 *
 * @codingStandardsIgnoreEnd
 */
class SkipOnStartWith extends SkipOnBase {

  /**
   * {@inheritDoc}.
   */
  protected function compareValue($value, $skipValue, $equal = TRUE) {
    // @todo add case sensitive configuration.
    $result = strpos((string) $value, (string) $skipValue) === 0;
    if ($equal) {
      return $result;
    }

    return !$result;
  }

}
