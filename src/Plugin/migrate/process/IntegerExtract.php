<?php

namespace Drupal\migrate_process_extra\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Row;

/**
 * Extracts the integer from a string.
 *
 * Can be used to extract e.g. zip codes.
 *
 * 1234 -> 1234
 * "1234 City" -> 1234
 * "1234.5 City" -> 12345
 *
 * Example.
 * @code
 * process:
 *   field_address/postal_code:
 *     plugin: integer_extract
 *     source: zip_and_city_source
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "integer_extract"
 * )
 */
class IntegerExtract extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_numeric($value)) {
      return (int) $value;
    }
    if (is_string($value)) {
      return (int) filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    }
    else {
      throw new MigrateException(sprintf('%s is not a valid must be numeric or a string.', var_export($value, TRUE)));
    }
  }

}
