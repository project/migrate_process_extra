<?php

namespace Drupal\migrate_process_extra\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateException;
use Drupal\migrate\ProcessPluginBase;

/**
 * Generates a media entity from a previously migrated file and returns the media id.
 */
abstract class FileToMediaBase extends ProcessPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The File storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * The Media storage.
   *
   * @var \Drupal\media\MediaStorage
   */
  protected $mediaStorage;

  /**
   * The Media destination field.
   *
   * @var string
   */
  protected $field;

  /**
   * The Media destination bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The original file.
   *
   * @var \Drupal\file\Entity\File
   */
  protected $file;

  /**
   * The optional Media name (label).
   *
   * @var string
   */
  protected $name;

  /**
   * Constructs FileToMediaBase.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->fileStorage = $this->entityTypeManager->getStorage('file');
    $this->mediaStorage = $this->entityTypeManager->getStorage('media');
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!isset($this->configuration['field'])) {
      throw new MigrateException('The Media field cannot be empty.');
    }

    if (!isset($this->configuration['bundle'])) {
      throw new MigrateException('The Media bundle cannot be empty.');
    }

    $this->field = $this->configuration['field'];
    $this->bundle = $this->configuration['bundle'];

    $this->file = $this->fileStorage->load($value);
    if ($this->file === NULL) {
      throw new MigrateException('The original file does not exist.');
    }

    if (isset($this->configuration['name'])) {
      $nameField = $this->configuration['name'];
      if ($row->hasSourceProperty($nameField)) {
        $this->name = $row->getSourceProperty($nameField);
      }
    }
  }

  /**
   * Creates a Media from the original file field values.
   *
   * @param array $field_values
   *   Original file field values to be copied to the Media destination field.
   *
   * @return int|null
   */
  protected function createMediaFromFile(array $field_values) {
    // @todo review language.
    // @todo check already created media based on the file id.
    // @todo check if the bundle and the file exists from the definition.
    // @todo implement rollback subscriber.
    $media = $this->mediaStorage->create(
      [
        'bundle' => $this->bundle,
        // @todo check if owner id exists otherwise provide fallback.
        //   or add mapping for authors.
        // 'uid' => $this->file->getOwner()->id(),
        'uid' => 1,
        'status' => 1,
        'name' => !empty($this->name) ? $this->name : $this->file->label(),
        // @todo check other fields.
        $this->field => $field_values,
      ]
    );
    $media->save();
    return $media->id();
  }

  /**
   * Post processing operations.
   *
   * Optionally deletes the original file.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function postProcess() {
    if (
      isset($this->configuration['delete_original_file']) &&
      $this->configuration['delete_original_file']
    ) {
      $this->fileStorage->delete([$this->file]);
    }
  }

}
