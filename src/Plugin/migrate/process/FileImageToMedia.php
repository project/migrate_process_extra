<?php

namespace Drupal\migrate_process_extra\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Generates a media entity from an image file and returns the media id.
 *
 * Assumes that a previous migration from upgrade_d7_file has already been
 * executed.
 * The Media name is optional, there is a fallback to the original File name.
 *
 * @MigrateProcessPlugin(
 *   id = "file_image_to_media"
 * )
 *
 * This code is to be set in the process section of an entity that
 * references a media on the destination.
 *
 * @code
 *  field_media_reference_name:
 *    -
 *      plugin: sub_process
 *      source: field_name_from_d7_source
 *      process:
 *        target_id:
 *          -
 *            plugin: migration_lookup
 *            source: fid
 *            migration: upgrade_d7_file
 *          -
 *            plugin: file_image_to_media
 *            bundle: image
 *            field: field_media_file
 *            name: file/title
 *            delete_original: true
 * @endcode
 */
class FileImageToMedia extends FileToMediaBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    parent::transform($value, $migrate_executable, $row, $destination_property);
    // Set the image alt tag with a fallback.
    $alt = $row->getSourceProperty('alt');
    if (empty($alt)) {
      $alt = $this->file->label();
    }
    $fileFieldValues = [
      'target_id' => $this->file->id(),
      'alt' => $alt,
    ];
    $mediaId = $this->createMediaFromFile($fileFieldValues);
    $this->postProcess();
    return $mediaId;
  }

}
