<?php

namespace Drupal\migrate_process_extra\Plugin\migrate\process;

use Drupal\migrate\MigrateException;

/**
 * Skips if the User entity value is not unique.
 *
 * Valid values: 'mail', 'name'.
 *
 * @code
 *   mail:
 *     plugin: skip_on_user_duplicate_value
 *     source: email
 *     method: row
 *     value: mail
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "skip_on_user_duplicate_value"
 * )
 */
class SkipOnUserDuplicateValue extends SkipOnBase {

  /**
   * {@inheritDoc}.
   */
  protected function compareValue($value, $skip_value, $equal = TRUE) {
    switch ($skip_value) {
      case 'mail':
        return user_load_by_mail(trim($value));
      case 'name':
        return user_load_by_name(trim($value));
    }
    throw new MigrateException(
      $this->t('@plugin_id plugin value is not valid. Possible values: mail or name.', [
        '@plugin_id' => $this->pluginId,
      ])
    );
  }

}
