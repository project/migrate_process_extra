<?php

namespace Drupal\migrate_process_extra\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\path\Plugin\migrate\source\d7\UrlAlias;

/**
 * Custom migration source to create redirects from Drupal 7 path aliases.
 *
 * @see https://deninet.com/blog/2018/04/22/migrating-path-aliases-drupal-8-redirects-part-2
 *
 * @MigrateSource(
 *   id = "d7_node_alias_to_redirect",
 *   source_module = "node",
 * )
 */
class NodeAliasToRedirect extends UrlAlias {

  /**
   * {@inheritDoc}.
   */
  public function query() {
    // Get the database query from the UrlAlias class.
    $query = parent::query();
    // Add condition to filter for only node paths.
    $query->condition('ua.source', 'node/%', 'LIKE');
    return $query;
  }

  /**
   * {@inheritDoc}.
   */
  public function prepareRow(Row $row) {
    // Get the source field from the row.
    $source = $row->getSourceProperty('source');
    // If it matches node/{nid}.
    if (preg_match('/node\/[0-9]+/', $source)) {
      // Get the node id from the string.
      $nid = substr($source, 5);
      // Provide it to the migration as the "nid" field.
      $row->setSourceProperty('nid', $nid);
    }
    return parent::prepareRow($row);
  }

}
